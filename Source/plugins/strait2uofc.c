#include "../api.h"

struct tm *tmnow;
float salary = 25000.00;
float rent   = 450.00;
int educated = 0;
int house    = 0;

//How much does he have after 75 years??
uintmax_t duration = 76.2*365.25*24*3600;

int initially(time_t t)
	{
	//Today, Ryan has this much mooney. 
	new_account(1, "cash",             0);
	new_account(1, "checking",   000000);
	new_account(1, "savings",    000000);
	
	//This is what he'll earn from
	new_account(4, "job",   999999999999);
	
	//This is what he spends it on
	new_account(5, "cellphone",0);
	new_account(5, "dental",0);
	new_account(5, "entertainment",0);
	new_account(5, "groceries",0);
	new_account(5, "lost",0);
	new_account(5, "meds",0);
	new_account(5, "rent",0);
	new_account(5, "shoes",0);
	new_account(5, "stuff",0);
	new_account(5, "transit",0);
	}

int daily(time_t t)
	{
	tmnow = localtime(&t);
	}

int monthly(time_t t)
	{
	//Every month, if his checking is low, he tops it up.
	if(account[1].balance <= 4000000)
		xfer("savings", "checking", (4000000-account[1].balance)/1000.0, t);
	
	//If he has excess money in his checking account, he puts it away in savings
	if(account[1].balance >= 4000000)
		xfer("checking", "savings", (account[1].balance-4000000)/1000.0, t);
	
	//He always ensures he has at least $2000 cash. 
	if(account[0].balance <= 2000000)
		xfer("checking", "cash", (2000000-account[0].balance)/1000.0, t);
	
	//He then spends this much on various things
	xfer("cash", "entertainment", 365.25, t);
	xfer("cash", "groceries", 150.00, t);
	xfer("cash", "lost", 158.20, t);
	xfer("cash", "meds", 150.00, t);
	xfer("cash", "rent", rent, t);
	xfer("cash", "transit", 80.00, t);
	}

int annually(time_t t)
	{
	if((account[2].balance > 125250000) && (educated == 0))
		{
		salary = 0;
		xfer("savings", "lost", 44000.00, t);
		educated = 2;
		printf("Went to UofC!\n");
		}
	
	//If he's saved to live to 100, he'll retire. 
	if(account[2].balance >= 16250000*(202-tmnow->tm_year))
		{
		printf("Retired!\n");
		exit(0);
		}
	
	//Afterwards, he'll get a higher paying one.
	if((educated == 2) && (tmnow->tm_year >= 140)) salary = 57200.00;
	
	//If he saves up for a house...
	if((account[2].balance > 400000000) && (house == 0))
		{
		xfer("savings", "lost", 400000.00, t);
		rent = 200.0;
		house++;
		}
	
	//He earns this much every year.
	xfer("job", "checking", salary, t);
	
	//Every year, he spends this much moomey.
	xfer("cash", "cellphone", 100.00, t);
	xfer("cash", "shoes", 250.00, t);
	}

int conclusively(time_t t)
	{
	print_time(t);
	print_balance("cash");
	print_balance("checking");
	print_balance("savings");
	printf("\n");
	print_balance("rent");
	print_balance("entertainment");
	print_balance("lost");
	print_balance("meds");
	print_balance("groceries");
	print_balance("transit");
	}

