// === LIBRARIES ===

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stddef.h>

// === 	VARIABLES ===

// A prototypical account as defined in double-entry bookkeeping. 
struct accountstruct
	{
	char type;		//Can be any of asset, liability, equity, revenue or expense.
	char name[256];		//The given name of the account. Examples include chequing, etc. 
	uintmax_t idnum;	//All accounts need an ID number for the general journal and ledger. 
	intmax_t balance;	//The value of the account. 
	};
struct accountstruct *account;
uintmax_t accounts = 0;

//A prototypical journal entry as defined in double-entry bookkeeping.
struct journalstruct
	{
	uintmax_t date;		//The Unix timestamp of the transaction.
	uintmax_t a_idf;	//The account ID number, from.
	uintmax_t a_idt;	//The account ID number, to. 
	uintmax_t t_id;		//A unique transaction ID number. 
	uintmax_t amount;	//The amount being debited or credited. 
	uintmax_t balance;	//The amount in the referenced account post-transaction. 
	char type;		//1 = debit (adds to assets, subs from liabilities and equity)
	};
struct journalstruct *journal_entry;
uintmax_t journal_entries = 0;

// === FUNCTIONS ===

/*

Return codes:
1 - Unable to allocate memory. 
2 - Account not found. 
3 - Insufficient funds. 
*/

//Creates a new account. Returns 1 on error.
int new_account(char type, char *name, uintmax_t balance)
	{
	int sum = 0;
	
	//Allocate our memory
	account = realloc(account, (accounts +1) * sizeof(struct accountstruct));
	
	//If this fails, stop and halt. 
	if(account == NULL) exit(1);
	
	//Otherwise, copy our parameters to our new account.
	accounts++;
	account[accounts -1].type = type;
	strncpy(account[accounts -1].name, name, 256);
	account[accounts -1].balance = balance;
	
	//Finally, assign an ID number to the account.	
	//To do this, we take the sum of all accounts of the same type.
	for(int C = 0;C < accounts;C++) if(account[C].type == type) sum++;
	
	//Assign our account this sum, plus 1. 
	account[accounts -1].idnum = sum +1;
	
	return 0;
	}

//Print the balance of our account.
int print_balance(char *name)
	{
	int FoundAccount = -1;
	
	//for every account that exists
	for(int C = 0;C < accounts;C++)
		{
		//Find the one with the same name. 
		if(strcmp(account[C].name, name) == 0) FoundAccount = C;
		}
	
	//If we found it, print our balance.
	if(FoundAccount > -1)
		{
		printf("%24.2f\n", (float)account[FoundAccount].balance/1000.0);
		}
	else
		{
		// Or else, stop and halt. 
		exit(2);
		}
	
	return 0;
	}

//Print the time
void print_time(time_t t)
	{
	struct tm *tmnow;
	char ftime[80];
	tmnow = localtime(&t);
	strftime(ftime, 80, "%c", tmnow);
	printf("%s\n", ftime);
	}

//Transfer money from one account to the next
int xfer(char *fr, char *to, float amount, time_t t)
	{
	int ito = -1;
	int ifr = -1;
	intmax_t qty = amount * 1000.0;
	char buffer[256];
	struct tm *tmnow;
	
	//Search all accounts for the given name. If found, change ito and ifr to their index.
	for(int C = 0;C < accounts;C++) if(strcmp(account[C].name, to) == 0) ito = C;
	for(int C = 0;C < accounts;C++) if(strcmp(account[C].name, fr) == 0) ifr = C;
	
	//If all accounts were found
	if((ito > -1) && (ifr > -1))
		{
		//And we have sufficient funds
		if((account[ifr].balance -qty) >= 0)
			{
			//Perform the transaction
			account[ifr].balance -= qty;
			account[ito].balance += qty;
			
			//Record our transaction in our journal
			tmnow = localtime(&t);
			strftime(buffer, 256, "%c", tmnow);
			printf("%s, %8.8s -> %8.8s, %10.2f, %10.2f\n",
			buffer, fr, to, amount, account[ifr].balance/1000.0);
			
			return 0;
			}
		else
			{exit(3);}
		}
	else
		{exit(2);}
	}


