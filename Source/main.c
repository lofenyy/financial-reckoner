#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <dlfcn.h>
#include <stdlib.h>

int main(int *argc, char **argv)
	{
	void* plugin = NULL;
	int (*initially)();
	int (*annually)();
	int (*monthly)();
	int (*daily)();
	int (*conclusively)();
	time_t now;
	struct tm *tmnow;
	time_t then;
	struct tm *tmthen;
	uintmax_t duration;
	
	// --- Load our selected Plugin
		
	plugin = dlopen(argv[1], RTLD_NOW);
	
	if(plugin == NULL)
		{
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
		}
	
	//Load our symbols
	initially	= (int (*)(time_t))    dlsym(plugin, "initially");
	annually	= (int (*)(time_t))    dlsym(plugin, "annually");
	monthly		= (int (*)(time_t))    dlsym(plugin, "monthly");
	daily		= (int (*)(time_t))    dlsym(plugin, "daily");
	conclusively	= (int (*)(time_t))    dlsym(plugin, "conclusively");
	duration	= (uintmax_t)	       dlsym(plugin, "duration");
	
	// --- Configure timing variables
	time(&then);
	time(&now);
	
	tmnow = localtime(&now);
	
	int oldyear  = tmnow->tm_year;
	int oldmonth = tmnow->tm_mon;
	int oldday   = tmnow->tm_mday;
	
	// --- Begin our routine
	
	initially(now);
	
	for(;now < (then+duration);now += 43200)
		{
		// --- Time rules ---
		struct tm *tmnow = localtime(&now);
		
		// --- Annual Rules ---
		if(tmnow->tm_year != oldyear)
			{
			oldyear = tmnow->tm_year;
			annually(now);
			}
		
		// --- Monthly Rules ---
		if(tmnow->tm_mon != oldmonth)
			{
			oldmonth = tmnow->tm_mon;
			monthly(now);
			}
		
		// --- Daily Rules ---
		if(tmnow->tm_mday != oldday)
			{
			oldday = tmnow->tm_mday;
			daily(now);
			}
		}
	
	// --- Finish up
	conclusively(now);
	fflush(stdout);
	return 0;
	}

